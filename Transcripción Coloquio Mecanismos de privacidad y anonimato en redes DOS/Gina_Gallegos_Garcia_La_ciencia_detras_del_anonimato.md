# La ciencia detrás del anonimato #
## _Gina Gallegos García_ ##

Un sistema de comunicaciones está dado por la parte que transmite el mensaje (entidad emisora) y la parte que recibe el mensaje (entidad receptora), esto a través de un canal que puede ser alámbrico o inalámbrico, pero detrás de toda comunicación siempre existen agentes que tratan de obtener la información que viaja a través del medio de comunicación, una forma de evitar que la información sea interceptada por terceros es protegiéndola.
Desde el comienzo de la escritura, en diversas ocasiones de la historia se ha necesitado preservar la confidencialidad de la información, ya que es de suma importancia que el mensaje se mantenga entre el transmisor y el receptor.

Existen diversas formas de proteger la información, pero hay cuatro servicios básicos que son confidencialidad, autenticidad, integridad y no repudio. El primero consiste en que el transmisor va a encriptar la información de tal forma que una tercera entidad no pueda entender lo que en realidad se está transmitiendo, esto quiere decir que la información a través del canal no va a viajar como texto plano y que solo el receptor va a ser conocedor de la forma en como desencriptarlo.
La integridad es el servicio en el que se asegura que ninguna tercera entidad pueda modificar el mensaje sin que este cambio sea detectado por el emisor o el receptor. En otras palabras, la meta de este servicio es que el mensaje llegue al receptor tal cual fue transmitido por el transmisor.
El objetivo de la autenticación es que de ninguna forma algún tercero suplante a alguno de los participantes del proceso de la comunicación. 
El servicio por medio del cual no se pueda negar la ejecución de una acción, es el no repudio. En este servicio se necesita la asistencia de una tercera entidad confiable que ayude a tener una buena comunicación.

Los servicios de seguridad previamente mencionados pueden combinarse o adecuarse para obtener otros servicios de seguridad de la información, ejemplo de esto es que el servicio de autentificación entre dos entidades se puede adaptar a tres entidades para obtener el servicio de anonimato, este servicio mantiene secreta la identidad del usuario. Esto quiere decir que no haya forma de asociar a la entidad transmisora con la información que esta transmite sobre el canal de información.
La criptografía es la ciencia que se encarga de estudiar las comunicaciones secretas, en términos específicos, se ocupa de estudiar algoritmos y técnicas matemáticas relacionadas con los aspectos de seguridad de la información, tales como confidencialidad, integridad, autenticación y no repudio.

La historia de esta ciencia se podría dividir en cuatro vertientes:

●	Criptografía clásica.
Se basaba en códigos de encriptación secretos, esta cualidad era la que aseguraba la seguridad y era de uso exclusivo para la milicia.
Existen dos elementos que fueron fundamentales para el desarrollo de la criptografía clásica, que son las sustituciones y las permutaciones, en la primera se sustituye un símbolo de algún alfabeto por algún otro símbolo y en la segunda se revuelve el mensaje, pero siempre siguiendo alguna secuencia.

●	Transición entre la criptografía clásica y la moderna.
Un ejemplo de este cambio es la máquina enigma, ya que esta no entra en ninguna de las dos vertientes, ya que la funcionalidad radica en técnicas que se utilizaban en la criptografía clásica, sin embargo, el desarrollo de esta máquina apunta hacia el desarrollo de las computadoras.

●	Criptografía moderna.
Esta se da a partir de la invención de la computadora, en la que se empiezan a trabajar con bits y bytes, se formalizan los conceptos clásicos gracias al matemático Shannon, se renombran como difusión y confusión.
La información se empieza a manipular a nivel de bloques, que es cuando se agrupa la información en conjuntos de bits y también se manipula a nivel de flujo.

●	Criptografía cuántica.
Nace a partir del problema en el que empiezan las primeras amenazas para la criptografía moderna, en las que se utiliza la física para poder desencriptar los mensajes.
Se utilizan fotones para transmitir información a través de fibra óptica.

Las primitivas criptográficas son abstracciones que nos permiten llegar a lo servicios de seguridad presentados con anterioridad, estas se pueden dividir como primitivas sin llave, primitivas con llave simétrica y primitivas con llave asimétrica, las más conocidas son las primitivas de cifrado, las primitivas de firma y las primitivas de hash.
Las primeras son aquellas que no utilizan algún tipo de llave para encriptar la información, las segundas son aquellas que utilizan una llave para encriptar la información, al ser simétricas quiere decir que tanto el receptor como el transmisor cuentan con la misma llave de encriptación y, por último, las primitivas con llave asimétrica son similares a las anteriores, pero el receptor y el transmisor cuentan con una llave diferente.

Los criterios para evaluar las primitivas criptográficas son el nivel de seguridad, que se refiere al número de operaciones que se requieren para poder obtener el texto original, una vez codificado, la funcionalidad, que se refiere a la adaptabilidad de la primitiva en cualquier dispositivo en el que se desee realizar una comunicación, los métodos de operación, que dependiendo de la configuración de las primitivas, pueden ofrecer diferentes tipos de servicios de seguridad, el desempeño, que está enfocado en la eficiencia y la facilidad de implementación.

Una llave es un elemento secreto que se utiliza para transformar la información.
En las firmas digitales están enfocadas a servicios de identificación, una firma digital es una cadena de datos a asociados a un mensaje con la entidad emisora del mismo. Un esquema de firma está formado por un algoritmo de generación de firma y uno de verificación.
Un medio por el cual se puede mantener la anonimidad, son las firmas ciegas, son un mecanismo que va más allá de preservar el servicio de autentificación y no repudio. Combina un protocolo con un esquema de firma digital, su propósito consiste en que el firmante desconozca el mensaje que está firmando al igual que la firma y que este sea incapaz de asociar el mensaje firmado con el transmisor de dicho mensaje. Esto se puede ver implementado en votaciones electrónicas y en el dinero electrónico.
__Gunnar:__ Bueno pues para tener un poco más de tiempo para las preguntas y
demás vamos a arrancar un poco antes de lo que dicta el programa y vamos a
intentar llevar todo el programa según está estipulado. Estamos unos 15 minutos
adelantados, lo cual me parece bien. Me da mucho gusto presentarles a __Roger
Dingledine__ que es el fundador del proyecto TOR y pues el sabrá decirles.

__Roger:__ Ok, I'm sorry but don't speak English. I will be try to speak slowly
and i heard there are this app to traduce from your phone to listen to the nice
people from the back on FM radio. So, if you need help making that work ask
Gunnar. He'll go back and be one of the two nice people translating  me. If you
bang on the window slow down and speak more slowly. Also, if I'm talking to
quickly please stop me and I'll slow down and try to be clearer.

I'm Roger, i work on the project TOR and I'm going to tell you a bunch of
different things about TOR and we can talk about more after words. I'm going
try to give a lot of different things to think about. TOR is a non-profit  in
the USA, it's a foundation charity and it's also a program that you can use to
have more safety on the internet, it's a network of volunteers around the world
who run relays help wrap TOR traffic. It's also a bunch of people around the
world. TOR is a community of researches, developers and users?16.16 teaching
everybody why privacy matters. The found part on my perspective every city
where I go to has a research group at the university doing research on TOR.
There are people at this university thinking about how improve TOR performance
or how to improve TOR security of how to analyse this.

TOR is a program that you can install on your computer, usually TOR Browser is
the way to install it and the idea is you can browse the web without the people
watching you, learning what web sites where going to and without people on the
other side learning where in the world you are. Without any single point in the
middle being to track what we are doing. So, that's the basic idea for what TOR
tries to prove.

We have some number of users, it's an anonymized system so it's a bit hard to
know exactly how users we have but we have something like 2,000,000 people
using TOR everyday. There's a research paper that came out recently saying that
there are more like eight to ten million daily users at TOR. So it's a huge and
growing community of people all around the world how care about privacy. We are
in the engineer school so lets start by thinking about the thread move?? [17.44].
What we can try to protect? What we are worry about?

So we have some user called Alice and she's trying to ripe some website called
Bob. Where can be the char?? be? [17.55] What we are worry about? So, maybe the
attacker is watching Alice's local network, maybe is something using the
wireless in the build right now or maybe is in the local telephone company,
that's one option, and another option is maybe the attacker is on the website
side, maybe there are watching with your weaks?? [18.15] and he want to know
how is connecting to these weaks?? or maybe there are in the destination, maybe
is cnn.com and they want to know how is connecting to the website so they can 
analyze to compare. 

Am I speaking well enough?, to slowly, clear enough? I guess that people for
how the answer no wont to be know in your heads cause don't understand what I 
say. Perfect

Or maybe the attacker is somewhere in the middle, maybe is att & t or horizon or
the Mexican telephone backbone and there are try to watch the hole thing and
learn how is talking in the grow??[18.56]. So, there are different places that
the attacker can be and we need to build the system that can keep you safe as
possible when people are trying to attack where your traffic is.

And other keep??[19.11] point. Anonymity is not encryption. A lot of people attach
a lot companies and they say "we don't need TOR because we use VPN we use
encryption so we are safe" and the problem there is somebody watching your
traffic is this cause effort?? [19.28], encryption is good, you should use
encryption but they still learn who you are talking to, when you are talking to
them , who much you are saying and all of the intelligent agencies, they don't
try to break encryption. No body try to breaks encryption anymore, it's all
about: let's build a social graph with how is talking with how and then find 
the person in the middle and then will break in their house and change the lan
or something?? [19.55]. So, the social graph is where the interesting thing were
all of the intelligent agencies they don't try to break encryption they are
look at this called meta data. This is a really creepy picture [20.17]
